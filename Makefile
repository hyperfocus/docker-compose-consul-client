consul-client: consul-client-compose

consul-client-compose:
	cd compose && docker-compose -f consul-client.yml up -d

consul-client-run:
	cd run && bash consul-client-run.sh
